<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

$installer = new Mage_Core_Model_Resource_Setup('core_setup');

$installer->startSetup();

$installer->getConnection()->raw_query("DROP TABLE IF EXISTS ulula_meli_price;");

$table = $installer->getConnection()
    ->newTable('ulula_meli_price')
    ->addColumn('sku', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false,
        'primary'   => true,
        ), 'Sku')
    ->addColumn('pid', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
        'primary'   => true,
        ), 'Product Id');

$installer->getConnection()->createTable($table);

$groupName = 'MeLi Group Price';

$group = Mage::getModel('customer/group')->load($groupName, 'customer_group_code');

if(!$group->getCustomerGroupId()){
	$installer->getConnection()->insert($installer->getTable('customer/customer_group'), array(
	    'customer_group_code'   => $groupName,
	    'tax_class_id'          => 3
	));
}


$sql = 'DROP TRIGGER IF EXISTS meli_price_update; CREATE TRIGGER meli_price_update AFTER UPDATE ON catalog_product_index_price FOR EACH ROW BEGIN INSERT INTO ulula_meli_price (sku, pid) VALUES ((SELECT SKU FROM catalog_product_entity  WHERE entity_id = NEW.entity_id), NEW.entity_id) ON DUPLICATE KEY UPDATE ulula_meli_price.sku=ulula_meli_price.sku, ulula_meli_price.pid=ulula_meli_price.pid;
    END; ';

$installer->getConnection()->raw_query($sql);

$sql = 'DROP TRIGGER IF EXISTS meli_price_insert; CREATE TRIGGER meli_price_insert AFTER INSERT ON catalog_product_index_price FOR EACH ROW BEGIN INSERT INTO ulula_meli_price (sku, pid) VALUES ((SELECT SKU FROM catalog_product_entity  WHERE entity_id = NEW.entity_id), NEW.entity_id) ON DUPLICATE KEY UPDATE ulula_meli_price.sku=ulula_meli_price.sku, ulula_meli_price.pid=ulula_meli_price.pid;
    END; ';

$installer->getConnection()->raw_query($sql);

$installer->endSetup();