<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

$installer = $this;
 
$installer->startSetup();
 
$table = $installer->getConnection()
    ->newTable($installer->getTable('ulula_mercadolibre/order'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Id')
    ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => true,
        ), 'Order Id')
    ->addColumn('meli_order_id', Varien_Db_Ddl_Table::TYPE_BIGINT, null, array(
        'nullable'  => false,
        ), 'Meli Order Ïd')
    ->addColumn('payment_id', Varien_Db_Ddl_Table::TYPE_BIGINT, null, array(
        'nullable'  => false,
        ), 'Payment Id')
    ->addColumn('shipping_id', Varien_Db_Ddl_Table::TYPE_BIGINT, null, array(
        'nullable'  => true,
        ), 'Shipping Id')
    ->addColumn('shipping_mode', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable'  => true,
        ), 'Shipping Mode')
    ->addColumn('meli_order_status', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable'  => false,
        ), 'Meli Order Status')
    ->addColumn('meli_order_data', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable'  => false,
        ), 'Meli Order Data');
$installer->getConnection()->createTable($table);
 
$installer->endSetup();