<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
$meliAttributes = array(
    'meli_ship_mode'=> array('label'=>'Meli Shipping Mode', 'input'=>'select', 'type'=>'int', 'options' => array('me2','custom')
    ),
    'meli_local_pick_up'=> array('label'=>'Meli local pick up', 'input'=>'select', 'type'=>'int', 'source' => 'eav/entity_attribute_source_boolean'
    ),
    'meli_free_shipping'=> array('label'=>'Meli free shipping', 'input'=>'select', 'type'=>'int', 'source' => 'eav/entity_attribute_source_boolean'
    )
);

for ($i=1; $i < 11 ; $i++) { 
    $meliAttributes['meli_shipping_custom_desc'.$i] = array('label'=>'Meli shipping custom desc'.$i, 'input'=>'text', 'type'=>'text');
     $meliAttributes['meli_shipping_custom_cost'.$i] = array('label'=>'Meli shipping custom cost'.$i, 'input'=>'price', 'type'=>'decimal', 'backend'=>'catalog/product_attribute_backend_price');
}
foreach ($meliAttributes as $key => $value) {
    $attribute = array(
        'group'           => Ulula_Mercadolibre_Helper_Attribute::ATTR_GROUP,
        'label'           => $value['label'].Mage::helper('ulula_mercadolibre/attribute')
                                                ->getAttributeLabelSuffix(),
        'input'           => $value['input'],
        'type'            => $value['type'],
        'required'        => '0',
        'visible_on_front'=> '0',
        'filterable'      => '0',
        'searchable'      => '0',
        'comparable'      => '0',
        'user_defined'    => '1',
        'is_configurable' => '0',
        'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note'            => '',
    );
    if(isset($value['options']) ){
        $attribute['option'] = array('values'=>$value['options']);
    }
    if(isset($value['source'])){
        $attribute['source'] = $value['source'];
    }
    if(isset($value['backend'])){
        $attribute['backend'] = $value['backend'];
    }
    $installer->addAttribute('catalog_product', $key, $attribute);
}

    $resource = Mage::getSingleton('core/resource');
    $writeConnection = $resource->getConnection('core_write');
    $sql = "UPDATE catalog_eav_attribute SET is_configurable = 0 WHERE attribute_id in (SELECT attribute_id FROM eav_attribute as a WHERE attribute_code like 'meli_%');";
    $writeConnection->query($sql);

$installer->endSetup();