<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
$meliAttributes = array(
    'meli_publish'=> array('label'=>'Publish', 'input'=>'select', 'type'=>'int', 'source' => 'eav/entity_attribute_source_boolean'
    ));
foreach ($meliAttributes as $key => $value) {
    $attribute = array(
        'group'           => Ulula_Mercadolibre_Helper_Attribute::ATTR_GROUP,
        'label'           => $value['label'].Mage::helper('ulula_mercadolibre/attribute')
                                                ->getAttributeLabelSuffix(),
        'input'           => $value['input'],
        'type'            => $value['type'],
        'required'        => 0,
        'visible_on_front'=> 0,
        'filterable'      => 0,
        'searchable'      => 0,
        'comparable'      => 0,
        'user_defined'    => 1,
        'is_configurable' => 0,
        'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'note'            => '',
    );
    if(isset($value['options']) ){
        $attribute['option'] = array('values'=>$value['options']);
    }
    if(isset($value['source'])){
        $attribute['source'] = $value['source'];
    }
    $installer->addAttribute('catalog_product', $key, $attribute);
}

$installer->endSetup();