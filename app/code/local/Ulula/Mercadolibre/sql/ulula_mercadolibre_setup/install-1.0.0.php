<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

$installer = $this;
 
$installer->startSetup();
 
$table = $installer->getConnection()
    ->newTable($installer->getTable('ulula_mercadolibre/item'))
    ->addColumn(
        'id', 
        Varien_Db_Ddl_Table::TYPE_INTEGER, 
        null, 
        array(
            'identity'  => true,
            'unsigned'  => true,
            'nullable'  => false,
            'primary'   => true
            ), 
        'Id'
    )
    ->addColumn(
        'meli_id', 
        Varien_Db_Ddl_Table::TYPE_VARCHAR, 
        null, 
        array(
            'nullable'  => false,
            ), 
        'Meli Id'
    )
    ->addColumn(
        'title', 
        Varien_Db_Ddl_Table::TYPE_VARCHAR, 
        null, 
        array(
            'nullable'  => false,
            ), 
        'Title'
    )
    ->addColumn(
        'permalink', 
        Varien_Db_Ddl_Table::TYPE_VARCHAR, 
        null, 
        array(
            'nullable'  => false,
            ), 
        'Link'
    )
    ->addColumn(
        'sku', 
        Varien_Db_Ddl_Table::TYPE_VARCHAR, 
        null, 
        array(
            'nullable'  => true,
            ), 
        'Sku'
    )
    ->addColumn(
        'info', 
        Varien_Db_Ddl_Table::TYPE_VARCHAR, 
        null, 
        array(
            'nullable'  => true,
            ), 
        'Info'
    )
    ->addColumn(
        'status', 
        Varien_Db_Ddl_Table::TYPE_VARCHAR, 
        null, 
        array(
            'nullable'  => false,
            ), 
        'Status'
    )
    ->addColumn(
        'sync', 
        Varien_Db_Ddl_Table::TYPE_INTEGER, 
        null, 
        array(
            'nullable'  => false,
            ), 
        'Sync'
    );

$installer->getConnection()->createTable($table);
 
$installer->endSetup();