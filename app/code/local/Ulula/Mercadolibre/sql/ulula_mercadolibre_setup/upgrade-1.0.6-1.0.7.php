<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

$installer = new Mage_Core_Model_Resource_Setup('core_setup');

$installer->startSetup();


$sql = 'DROP TRIGGER IF EXISTS meli_stock_insert; CREATE TRIGGER meli_stock_insert AFTER INSERT ON cataloginventory_stock_item FOR EACH ROW BEGIN INSERT INTO ulula_meli_stock (sku, qty) VALUES ((SELECT SKU FROM catalog_product_entity  WHERE entity_id = NEW.product_id), NEW.qty) ON DUPLICATE KEY UPDATE ulula_meli_stock.sku=ulula_meli_stock.sku,ulula_meli_stock.qty=NEW.qty;
    END; ';

$installer->getConnection()->raw_query($sql);

$installer->endSetup();