<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Model_Payment_Mercadolibre extends Mage_Payment_Model_Method_Abstract
{

	protected $_code = 'mercadolibre';

	protected $_infoBlockType = 'ulula_mercadolibre/payment_info_mercadolibre';
	protected $_canUseCheckout = false;

	public function isAvailable($quote = NULL){
		return true;
	}

	public function assignData($data)
    {
    	if(isset($data['details']) && !empty($data['details']) ){
    		$this->getInfoInstance()->setAdditionalData(serialize($data['details']));
    	}
        return $this;
    }

}