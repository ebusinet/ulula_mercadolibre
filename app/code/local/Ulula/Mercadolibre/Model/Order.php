<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Model_Order extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('ulula_mercadolibre/order');
    }

    public function setOrderData($data){
    	$this->setData('meli_order_id', $data['order_id']);
    	$this->setData('payment_id', $data['payment_id']);
    	$this->setData('shipping_id', $data['shipping_id']);
    	$this->setData('shipping_mode', $data['shipping_mode']);
    	$this->setData('meli_order_status', $data['payment_status']);
    	$this->setData('meli_order_data', json_encode($data));
    	$this->save();
    	return $this;
    }

}