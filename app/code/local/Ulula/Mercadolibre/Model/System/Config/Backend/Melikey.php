<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Model_System_Config_Backend_Melikey extends Mage_Core_Model_Config_Data
{

	protected function _beforeSave()
    {
    	if($this->getValue()){
	    	$result = Mage::helper('ulula_mercadolibre/api')->checkKey($this->getValue());
	        if(!is_array($result) && $this->getValue() != ''){
	        	throw new Exception($result);
	        }
        }
 	}

}