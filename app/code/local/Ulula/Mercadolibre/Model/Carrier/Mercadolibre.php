<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Model_Carrier_Mercadolibre extends Mage_Shipping_Model_Carrier_Abstract implements Mage_Shipping_Model_Carrier_Interface
{
	protected $_code = 'mercadolibre';

	public function collectRates(Mage_Shipping_Model_Rate_Request $request)
	{
		/** @var Mage_Shipping_Model_Rate_Result $result */
		$result = Mage::getModel('shipping/rate_result');
	 
	 	$shippingData = Mage::registry('shipping-data');
	 	if (!isset($shippingData['mode']))
	 		return false;
		$method = Mage::getModel('shipping/rate_result_method');

            $method->setCarrier($this->_code);
            $method->setCarrierTitle(Mage::helper('shipping')->__('Mercadolibre'));

            $method->setMethod('mercadoenvios');
            $method->setMethodTitle($shippingData['name']);

            $method->setPrice($shippingData['cost']);
            $method->setCost(0);

            $result->append($method);
	 	Mage::unregister('shipping-data');

		return $result;
	}

	public function getAllowedMethods()
    {
        return ['mercadoenvios'=>'MercadoEnvios'];
    }

    /**
     * Check if carrier has shipping tracking option available
     *
     * @return boolean
     */
    public function isTrackingAvailable()
    {
        return true;
    }

    /**
     * Check if carrier has shipping label option available
     *
     * @return boolean
     */
    public function isShippingLabelsAvailable()
    {
        return true;
    }
}