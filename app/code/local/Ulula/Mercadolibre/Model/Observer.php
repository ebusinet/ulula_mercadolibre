<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Model_Observer extends Mage_Core_Model_Abstract
{
	/**
	 * Flag to stop observer executing more than once
	 *
	 * @var static bool
	 */
	static protected $_singletonFlag = false;

	/**
	 * This method will run when the product is saved from the Magento Admin
	 * Use this function to update the product model, process the 
	 * data or anything you like
	 *
	 * @param Varien_Event_Observer $observer
	 */
	public function saveProductTabData(Varien_Event_Observer $observer)
	{
		if (!self::$_singletonFlag) {
			self::$_singletonFlag = true;
			
			$product = $observer->getEvent()->getProduct();
		
			try {
				/**
				 * Perform any actions you want here
				 *
				 */
				$data = $this->_getRequest()->getPost();
				// Zend_debug::dump($data);die;
				// $product->setData('meli_category_id','hola');
				/**
				 * Uncomment the line below to save the product
				 *
				 */
				// $product->save();
			}
			catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
	}
     
	/**
	 * Retrieve the product model
	 *
	 * @return Mage_Catalog_Model_Product $product
	 */
	public function getProduct()
	{
		return Mage::registry('product');
	}
	
    /**
     * Shortcut to getRequest
     *
     */
    protected function _getRequest()
    {
        return Mage::app()->getRequest();
    }

    /**
     * Deprecated in version 1.0.9
     */
	public function checkPriceStock(){
		
	}

	protected function checkAttributes($product, $meliItem, $request=false){
		$meliTile = $meliItem->getTitle();
		$title = Mage::helper('ulula_mercadolibre/product')->getTitle($product);
		if($title && $title != $meliTile){
			$request['in_data']['title'] = $title;
		}
		$meliDescription = $meliItem->getDescription();
		$description = Mage::helper('ulula_mercadolibre/product')->getDescription($product);
		if($description && $description != $meliDescription){
			$request['in_data']['description'] = $description;
		}
		return $request;
	}

	/*
	* Returns product price for Mercadolibre
	* @param Mage_Catalog_Model_Product 
	* @return float
	*/
	protected function getProductPrice($product){
		$customer_group_code = Mage::helper('ulula_mercadolibre')->getGroupPriceName();
		$group = Mage::getModel('customer/group')->load($customer_group_code, 'customer_group_code');
		$product->setCustomerGroupId($group->getId());
		return $product->getFinalPrice();
	}

	public function checkStock(){
		$_helper = Mage::helper('ulula_mercadolibre/stock');
		$stockList = $_helper->getStockJson();
		if($_helper->isSynchroEnabled('stock') && ($stockList != false) ){
			foreach ($stockList as $stock) {
				Mage::helper('ulula_mercadolibre/api')
					->synchroStock(
						array(
							'stock_data'=>json_encode($stock),
							'offset'=> $_helper->getStockOffset()
						)
					);
			}
		}
	}

	public function checkPrice(){
		$_helper = Mage::helper('ulula_mercadolibre/price');
		$priceList = $_helper->getPriceJson();
		if($_helper->isSynchroEnabled('price') && ($priceList != false) ){
			foreach ($priceList as $price) {
				Mage::helper('ulula_mercadolibre/api')->synchroPrice(array('price_data'=>json_encode($price)));
			}
		}
	}

	protected function getProductsBySku($skuArray){
		$fields = ['name','price','special_price','group_price'];
		$collection = Mage::getResourceModel('catalog/product_collection')
		    ->addAttributeToSelect($fields) 
		    ->joinField('qty',
	                 'cataloginventory/stock_item',
	                 'qty',
	                 'product_id = entity_id',
	                 '{{table}}.stock_id=1',
	                 'left')
		    ->addAttributeToFilter(
		        'sku', array('in' => $skuArray)
		    )->load();
	    $collection->addFinalPrice();
	    return $collection;
	}

	public function getMeliItems(){
		return Mage::getResourceModel('ulula_mercadolibre/item_collection')
			->addFieldToFilter('sku', array('notnull' => true))
			->addFieldToFilter('status', array('neq' => 'closed'))
			->addFieldToFilter('sync', 1);
	}

	public function salesOrderShipmentSaveBefore($observer)
    {
        $shipment = $observer->getEvent()->getShipment();
        $order = $shipment->getOrder();
        $meliOrder = Mage::getModel('ulula_mercadolibre/order')->load($order->getId(), 'order_id');
        if($meliOrder->getShippingMode() == 'me2'){
        	$labelUrl = $this->getLabelUrl($meliOrder->getShippingId());
        	$pdfLabel = file_get_contents($labelUrl);
	        $shipment->setShippingLabel($pdfLabel);
	        $trackingUrl = $this->getLabelUrl($meliOrder->getShippingId(), 'tracking');
	        $tarcking = $this->getTracking($trackingUrl);
	        $track = Mage::getModel('sales/order_shipment_track')
	                        ->setNumber($tarcking) 
	                        ->setCarrierCode('mercadolibre') 
	                        ->setTitle('mercadolibre'); 
	        $shipment->addTrack($track);
        }
    }

    protected function getTracking($url){
    	$curl = curl_init($url);
	    if (FALSE === $curl)
	       throw new Exception('failed to initialize');
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    $result = curl_exec($curl);
	    if (FALSE === $result)
	        throw new Exception(curl_error($curl), curl_errno($curl));
	    if($result == 0){
	        throw new Exception(Mage::helper('ulula_mercadolibre')->__('Invalid Tracking'));
	    }
	    return $result;
    }

    protected function getLabelUrl($shippingId, $type='label'){
    	$authKey = Mage::helper('ulula_mercadolibre')->getAll2MeliKey();
    	return 'https://all2meli.ulula.net/meli/v1/shipping/'.$type.'?auth_key='.$authKey.'&shipping_id='.$shippingId;
    }

    /*
	* Add a new tab to categories to map meli categories
	* @return void
	*/
    public function addCategoryTab($observer){
    	$tabs = $observer->getTabs();
    	$layout = $observer->getLayout();
    	$tabs->addTab('meli', array(
		    'label'     => Mage::helper('ulula_mercadolibre')->__('Mercadolibre'),
		    'content'   => $tabs->getLayout()->createBlock('ulula_mercadolibre/adminhtml_catalog_category_tab_meli')->setTemplate('ulula/mercadolibre/categories.phtml')->toHtml()
		));
    }

}