<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Block_Payment_Info_Mercadolibre extends Mage_Payment_Block_Info
{
	protected $_issuer;
    protected $_status;

	protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('ulula/mercadolibre/payment/info/mercadolibre.phtml');
    }

    /**
     *
     * @return string
     */
    public function getIssuer()
    {
        if (is_null($this->_issuer)) {
            $this->_convertAdditionalData();
        }
        return $this->_issuer;
    }

    /**
     *
     * @return string
     */
    public function getStatus()
    {
        if (is_null($this->_status)) {
            $this->_convertAdditionalData();
        }
        return $this->_status;
    }

    /**
     *
     * @return Mage_Payment_Block_Info_Checkmo
     */
    protected function _convertAdditionalData()
    {
        $details = false;
        try {
            $details = Mage::helper('core/unserializeArray')
                ->unserialize($this->getInfo()->getAdditionalData());
        } catch (Exception $e) {
            Mage::logException($e);
        }
        if (is_array($details)) {
            $this->_issuer = isset($details['issuer']) ? (string) $details['issuer'] : '';
            $this->_status = isset($details['status']) ? (string) $details['status'] : '';
        } else {
            $this->_issuer = '';
            $this->_status = '';
        }
        return $this;
    }
}