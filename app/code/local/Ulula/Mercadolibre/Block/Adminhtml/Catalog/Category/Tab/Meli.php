<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Block_Adminhtml_Catalog_Category_Tab_Meli extends Mage_Adminhtml_Block_Widget_Grid
{
	protected $_category;

    public function __construct()
    {
        parent::__construct();
        $this->setShowGlobalIcon(true);
    }

    public function getCategory()
    {
        if (!$this->_category) {
            $this->_category = Mage::registry('category');
        }
        return $this->_category;
    }

    public function getMeliCategory(){
        $mageCategoryId = $this->getCategory()->getId();
        return Mage::getModel('ulula_mercadolibre/category')->load($mageCategoryId, 'mage_category_id');
    }

}