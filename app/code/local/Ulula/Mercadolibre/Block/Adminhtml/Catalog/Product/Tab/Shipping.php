<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Block_Adminhtml_Catalog_Product_Tab_Shipping extends Mage_Adminhtml_Block_Template
{

    const CUSTOM_SHIPPING_OPTIONS = 10;

	 protected $_product;
    /**
     * Set the template for the block
     *
     */
    public function _construct()
    {
        parent::_construct();
        if (!$this->_product || (!$this->_product instanceof Mage_Catalog_Model_Product)) {
            $this->_product = Mage::registry('current_product');
        }
        $this->setTemplate('ulula/mercadolibre/catalog/product/tab/shipping.phtml');
    }

    public function getProdcut()
    {
        return $this->_product;
    }

    public function getShippingModeInput($mode='me2'){
        $_productHelper = Mage::helper('ulula_mercadolibre/product');
        $currentMode = $_productHelper->getShippingMode($this->getProdcut());
        $modeValue = (int)$_productHelper->getAttributeValueFromLabel('meli_ship_mode', $mode);
        $checked = ($mode == $currentMode)?'checked="checked"':'';
        $show = ($mode=='custom');
        return '<input onclick="showCustomShipping('.$show.')" data="'.$currentMode.'" '.$checked.' type="radio" name="product[meli_ship_mode]" value="'.$modeValue.'">';
    }

    public function getLocalPickUpInput(){
        $_productHelper = Mage::helper('ulula_mercadolibre/product');
        $currentValue = (int)$this->getProdcut()->getMeliLocalPickUp();
        $options = array($this->__('No'), $this->__('Yes'));
        $html .= '<select id="meli_local_pick_up" name="product[meli_local_pick_up]" class="select">';
        foreach ($options as $key => $value) {
            $selected = ($currentValue == $key)?'selected="selected"':'';
            $html .= '<option '.$selected.' value="'.$key.'">'.$value.'</option>';
        }
        return $html.'</select>';
    }

    public function getCustomShippingInput(){
        $html = '';
        for ($i=1; $i <= self::CUSTOM_SHIPPING_OPTIONS ; $i++) { 
            $html .= '<tr class="meli-custom-shipping"><td class="label"><label for="">'.$this->__('Custom Shipping ').$i.'</label>
            </td>';
            $html .= '<td class="value"><input id="meli_shipping_custom_desc'.$i.'" name="product[meli_shipping_custom_desc'.$i.']" class=" input-text" value="'.$this->getProdcut()->getData('meli_shipping_custom_desc'.$i).'" type="text"></td>';
            $html .= '<td class="value"><input id="meli_shipping_custom_cost'.$i.'" name="product[meli_shipping_custom_cost'.$i.']" class=" input-text" value="'.$this->getProdcut()->getData('meli_shipping_custom_cost'.$i).'" type="text"></td>';
            $html .= '</tr>';
        }
        return $html;
    }

}