<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Block_Adminhtml_Catalog_Product_Tab_Store extends Mage_Adminhtml_Block_Template
{
	 protected $_product;
    /**
     * Set the template for the block
     *
     */
    public function _construct()
    {
        parent::_construct();
        if (!$this->_product || (!$this->_product instanceof Mage_Catalog_Model_Product)) {
            $this->_product = Mage::registry('current_product');
        }
        Mage::helper('ulula_mercadolibre/store')->checkStores();
        $this->setTemplate('ulula/mercadolibre/catalog/product/tab/store.phtml');
    }

    public function getProdcut()
    {
        return $this->_product;
    }

    public function getOptions(){
    	$html = '';
    	$options = Mage::helper('ulula_mercadolibre/attribute')->getAttrOptions('meli_stores');
    	$selectedOptions = $this->getProdcut()->getMeliStores();
    	$selectedOptionsArray = explode(',', $selectedOptions);
    	foreach ($options as $option) {
    		if($option['value'] != ''){
    			$value = $option['value'];
	    		$label = $option['label'];
	    		$selected = (in_array($option['value'], $selectedOptionsArray))?'selected=selected':'';
	    		$html .=  '<option '.$selected.' value="'.$value.'">'.$label.'</option>';
    		}
    	}
    	return $html;
    }
}