<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */
class Ulula_Mercadolibre_Block_Adminhtml_Meli_Catalog_Product_Renderer_Publish 
	extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row) 
    { 
        $value =  $row->getData($this->getColumn()->getIndex());
        return ($value)? $this->__('Yes')  :  $this->__('No');
    }

}
