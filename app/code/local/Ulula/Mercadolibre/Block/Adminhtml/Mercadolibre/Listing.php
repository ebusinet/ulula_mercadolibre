<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author    	Gaston De Marsico <gdemarsico@ulula.net>
 */
 
class Ulula_Mercadolibre_Block_Adminhtml_Mercadolibre_Listing extends Mage_Adminhtml_Block_Widget_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('ulula/mercadolibre/listing.phtml');
    }


    public function getIframeUrl(){
    	return '//all2meli.ulula.net/meli/list?key='.Mage::helper('ulula_mercadolibre')->getAll2MeliKey();
    }
}