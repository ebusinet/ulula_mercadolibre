<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author    	Gaston De Marsico <gdemarsico@ulula.net>
 */
 
class Ulula_Mercadolibre_Block_Adminhtml_Mercadolibre_Item_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
    {
        parent::__construct();
        $this->setId('mercadolibre_item_grid');
        $this->setDefaultSort('increment_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
 
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('ulula_mercadolibre/item_collection');
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }
 
    protected function _prepareColumns()
    {
        $helper = Mage::helper('ulula_mercadolibre');
 
        $this->addColumn('meli_id', array(
            'header' => $helper->__('Publication Id'),
            'index'  => 'meli_id'
        ));
 
        $this->addColumn('title', array(
            'header' => $helper->__('Title'),
            'index'  => 'title'
        ));

        $this->addColumn('sku', array(
            'header' => $helper->__('Sku'),
            'index'  => 'sku'
        ));
 
        $this->addColumn('permalink', array(
            'header'       => $helper->__('Link'),
            'index'        => 'permalink',
            'sortable'  => false,
            'filter'    => false,
            'width' => "500px",
            'renderer' => 'Ulula_Mercadolibre_Block_Adminhtml_Mercadolibre_Item_Grid_Renderer_Link',
        ));
 
        $this->addColumn('status', array(
            'header'       => $helper->__('Status'),
            'index'        => 'status',
            'type' => 'options',
            'options'=> ['active'=>$helper->__('active'), 'paused'=>$helper->__('paused'), 'closed'=>$helper->__('closed')],
            'renderer' => 'Ulula_Mercadolibre_Block_Adminhtml_Mercadolibre_Item_Grid_Renderer_Status',
        ));

        $this->addColumn('sync', array(
            'header'       => $helper->__('Sync'),
            'index'        => 'sync',
            'type' => 'options',
            'options'=> [0 => $helper->__('Disabled'), 1=>$helper->__('Enabled')],
            'renderer' => 'Ulula_Mercadolibre_Block_Adminhtml_Mercadolibre_Item_Grid_Renderer_Sync',
        ));

        $this->addColumn('action',
                array(
                    'header'    => $helper->__('Action'),
                    'width'     => '50px',
                    'type'      => 'action',
                    'getter'     => 'getId',
                    'actions'   => array(
                        array(
                            'caption' => $helper->__('Edit'),
                            'url'     => array('base'=>'*/*/edit'),
                            'field'   => 'id',
                            'data-column' => 'action',
                        )
                    ),
                    'filter'    => false,
                    'sortable'  => false,
                    'index'     => 'stores',
                    'is_system' => false,
            ));
 
        $this->addExportType('*/*/exportItemCsv', $helper->__('CSV'));
        $this->addExportType('*/*/exportItemExcel', $helper->__('Excel XML'));
 
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $helper = Mage::helper('ulula_mercadolibre');
        $this->setMassactionIdField('item_id_mass_action');
        $this->getMassactionBlock()->setFormFieldName('item');
         
        $syncState = [
            ['label'=>'', 'value'=>''],
            ['label'=>$helper->__('Disabled'), 'value'=>0],
            ['label'=>$helper->__('Enabled'), 'value'=>1]
        ];

        $statusValues = [
            ['label'=>'', 'value'=>''],
            ['label'=>$helper->__('Activate'), 'value'=>'active'],
            ['label'=>$helper->__('Pause'), 'value'=>'paused'],
            ['label'=>$helper->__('Close'), 'value'=>'closed'],
            ['label'=>$helper->__('Delete'), 'value'=>'delete'],
        ];

        $this->getMassactionBlock()->addItem('sync', [
            'label'=> $helper->__('Change sync'),
            'url'  => $this->getUrl('*/*/massSync', ['_current'=>true]),
            'additional' => [
                    'visibility' => [
                         'name' => 'sync',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => $helper->__('Sync'),
                         'values' => $syncState
                    ]
             ]
        ]);

        $this->getMassactionBlock()->addItem('status', [
            'label'=> $helper->__('Change status'),
            'url'  => $this->getUrl('*/*/massStatus', ['_current'=>true]),
            'additional' => [
                    'visibility' => [
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => $helper->__('Status'),
                         'values' => $statusValues
                    ]
             ],
            'confirm'  => Mage::helper('ulula_mercadolibre')->__('Are you sure?')
        ]);
        return $this;
    }
 
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}