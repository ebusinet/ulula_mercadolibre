<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */
 
class Ulula_Mercadolibre_Block_Adminhtml_Mercadolibre_Item_Edit extends Mage_Adminhtml_Block_Widget_Form_Container{

   public function __construct()
   {
        parent::__construct();
        $this->_objectId = 'id';
        //you will notice that assigns the same blockGroup the Grid Container
        $this->_blockGroup = 'ulula_mercadolibre';
        // and the same container
        $this->_controller = 'adminhtml_mercadolibre_item';
        //we define the labels for the buttons save and delete
        // $this->_removeButton('save');
        $this->_removeButton('delete');
        $this->_removeButton('reset');
        //$this->_updateButton('save', 'label','Save movie');
        //$this->_updateButton('delete', 'label', 'Delete movie');
    }

       /* Here, we look at whether it was transmitted item to form
        * to put the right text in the header (Add or Edit)
        */

    public function getHeaderText()
    {
        if( Mage::registry('item_data') && Mage::registry('item_data')->getId() )
         {
              return 'Edit a publication '.$this->htmlEscape( Mage::registry('item_data')->getId() );
         }
         else
         {
            // return 'Add a movie';
         }
    }
}