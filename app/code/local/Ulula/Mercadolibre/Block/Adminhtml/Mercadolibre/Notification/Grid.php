<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author    	Gaston De Marsico <gdemarsico@ulula.net>
 */
 
class Ulula_Mercadolibre_Block_Adminhtml_Mercadolibre_Notification_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
    {
        parent::__construct();
        $this->setId('mercadolibre_notification_grid');
        $this->setDefaultSort('increment_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
 
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('ulula_mercadolibre/notification_collection');
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }
 
    protected function _prepareColumns()
    {
        $helper = Mage::helper('ulula_mercadolibre');

        $this->addColumn('id', array(
            'header' => $helper->__('Id'),
            'index'  => 'id'
        ));
 
        $this->addColumn('meli_id', array(
            'header' => $helper->__('Publication Id'),
            'index'  => 'meli_id'
        ));
 
        $this->addColumn('type', array(
            'header' => $helper->__('Type'),
            'index'  => 'type'
        ));

        $this->addColumn('message', array(
            'header' => $helper->__('Message'),
            'index'  => 'message'
        ));
 
        $this->addColumn('timestamp', array(
            'header'       => $helper->__('Timestamp'),
            'index'        => 'timestamp'
        ));
 
        $this->addExportType('*/*/exportNotificationCsv', $helper->__('CSV'));
        $this->addExportType('*/*/exportNotificationExcel', $helper->__('Excel XML'));
 
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');
         
        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('tax')->__('Delete'),
            'url'  => $this->getUrl('*/*/massDelete', array('' => '')),        // public function massDeleteAction() in Mage_Adminhtml_Tax_RateController
            'confirm' => Mage::helper('tax')->__('Are you sure?')
        ));
         
        return $this;
    }

 
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}