<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */
 
class Ulula_Mercadolibre_Block_Adminhtml_Mercadolibre_Item_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
  {
     public function __construct()
     {
          parent::__construct();
          $this->setId('item_tabs');
          $this->setDestElementId('edit_form');
          $this->setTitle('Information');
      }

      protected function _beforeToHtml()
      {
          $this->addTab('form_section', array(
                   'label' => 'About the publication',
                   'title' => 'About the publication',
                   'content' => $this->getLayout()
                                     ->createBlock('ulula_mercadolibre/adminhtml_mercadolibre_item_edit_tab_form')
                                     ->toHtml()
        ));
        
        return parent::_beforeToHtml();
    }
}
