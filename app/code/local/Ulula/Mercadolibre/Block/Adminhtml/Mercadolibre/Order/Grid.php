<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author        Gaston De Marsico <gdemarsico@ulula.net>
 */
 
class Ulula_Mercadolibre_Block_Adminhtml_Mercadolibre_Order_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('mercadolibre_order_grid');
        $this->setDefaultSort('increment_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
 
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('ulula_mercadolibre/order_collection');
        $collection->getSelect()
            ->joinLeft(
                'sales_flat_order', 
                'main_table.order_id = sales_flat_order.entity_id', 
                array('increment_id')
            );  
        $this->setCollection($collection);
        $this->setDefaultSort('id');
        $this->setDefaultDir('desc');
        parent::_prepareCollection();
        return $this;
    }
 
    protected function _prepareColumns()
    {
        $helper = Mage::helper('ulula_mercadolibre');

        $this->addColumn(
            'id', array(
            'header' => $helper->__('Id'),
            'type' => 'number',
            'index'  => 'id'
            )
        );

        $this->addColumn(
            'order_id', array(
            'header' => $helper->__('Magento Order Id'),
            'type' => 'number',
            'index'  => 'order_id',
            'renderer'  => 'Ulula_Mercadolibre_Block_Adminhtml_Mercadolibre_Order_Grid_Renderer_Incremental',
            )
        );
 
        $this->addColumn(
            'meli_order_id', array(
            'header' => $helper->__('Mercadolibre Order Id'),
            'type' => 'number',
            'index'  => 'meli_order_id'
            )
        );
 
        $this->addColumn(
            'payment_id', array(
            'header' => $helper->__('Payment Id'),
            'type' => 'number',
            'index'  => 'payment_id'
            )
        );

        $this->addColumn(
            'shipping_id', array(
            'header' => $helper->__('Shipping Id'),
            'type' => 'number',
            'index'  => 'shipping_id'
            )
        );
 
        $this->addExportType('*/*/exportOrderCsv', $helper->__('CSV'));
        $this->addExportType('*/*/exportOrderExcel', $helper->__('Excel XML'));
 
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');      
        return $this;
    }
 
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

}