<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */
 
class Ulula_Mercadolibre_Block_Adminhtml_Mercadolibre_Item_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

  protected $_itemData;

  protected function _construct()
  {
        parent::_construct();
        $this->setTemplate('ulula/mercadolibre/items/form.phtml');
        $this->setDestElementId('edit_form');
        $this->setShowGlobalIcon(false);
    }

   protected function _prepareForm()
   {
       $form = new Varien_Data_Form();
       $this->setForm($form);
       $fieldset = $form->addFieldset('item_form',
                                       array('legend'=>'pub information'));

       $fieldset->addField('meli_id', 'text',
                       array(
                          'label' => 'Pub Id',
                          'class' => 'required-entry',
                          'required' => true,
                          'readonly'=> true,
                           'name' => 'meli_id',
                    ));

       $fieldset->addField('title', 'text',
                       array(
                          'label' => 'title',
                          'class' => 'required-entry',
                          'required' => true,
                          'readonly'=> true,
                           'name' => 'title',
                    ));

       $fieldset->addField('sku', 'text',
                       array(
                          'label' => 'SKU',
                          'class' => 'required-entry',
                          'required' => true,
                          // 'readonly'=> false,
                           'name' => 'sku',
                    ));
     if ( Mage::registry('item_data') )
     {
        $form->setValues(Mage::registry('item_data')->getData());
        $this->_itemData = Mage::registry('item_data');
      }
    
      return parent::_prepareForm();
     }

     public function getItemData(){
        if(!$this->_itemData) {
          $this->_itemData = Mage::registry('item_data');
        }
        return $this->_itemData;
     }

     public function getDatabyField($field = null){

      return ($field == null)? $this->getItemData()->getData()
          :$this->getItemData()->getData($field);
     }

     public function getInfo(){
      $info = json_decode($this->getDatabyField('info'), true);
      return $info;
     }

     public function getId(){
        return $this->getDatabyField('id');
     }

     public function getSku(){
        return $this->getDatabyField('sku');
     }

     public function getVariations(){
        $info = $this->getInfo();
        return $info['variations'];
     }

     public function getVariationsHtml(){
        $variations = $this->getVariations();
        $html ='';
        foreach ($variations as $variation) {
            $html .= '<div  class="left" style="width:100%">';
            $sku = $variation['seller_custom_field'];
            $html .= $this->getCombinations($variation);
            $html .= '<div class="left" style="padding:15px">';
            $html .= '<div>SKU</div>';
            $html .= '<div>'.$this->getProductLinkBySku($sku).'</div>';
            $html .= '</div></div>';
        }
        return $html;
     }

     protected function getProductLinkBySku($sku){
        if(!$sku)
          return $this->__('Invalid SKU');
        $productId = Mage::getModel('catalog/product')->getResource()->getIdBySku($sku);
        if(!$productId)
          return $this->__('Invalid SKU');
        $url = 'adminhtml/catalog_product/edit';
        return '<a target="_blank" href="'.Mage::helper('adminhtml')->getUrl($url, ['id'=>$productId]).' " >'.$sku.'</a>';
     }

     public function getCombinations($variation){
        $combinations = $variation['attribute_combinations'];
        $html = '';
        foreach ($combinations as $combination) {
          $html .= '<div class="left" style="padding:15px 15px 15px 0">
                      <div>'.$combination['name'].'</div>
                      <div>'.$combination['value_name'].'</div>
                    </div>';
        }
        return $html;
     }
}