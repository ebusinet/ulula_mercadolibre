<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author    	Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Helper_Image extends Ulula_Mercadolibre_Helper_Data
{

	const MAX_IMAGES = 6;

	protected $_counter = 0;

	protected function _getGalleryTableName(){
		return 'catalog_product_entity_media_gallery';
	}

	protected function _getGalleryValueTableName(){
		return 'catalog_product_entity_media_gallery_value';
	}

	protected function _getQuery($product){
		return 'SELECT * FROM '.$this->_getGalleryTableName().' as a LEFT JOIN '.$this->_getGalleryValueTableName().' as b ON a.value_id=b.value_id WHERE disabled = 0 AND a.entity_id ='.$product->getId().' ORDER BY position';
	}

	protected function _isValid($url){
		return (filter_var($url, FILTER_VALIDATE_URL) !== false);
	}

	/**
	* Returns media gallery images
	* @param Mage_Catalog_Model_Product $product
	* @param array $additionalImages 
	* @return  array
	*/
	public function getMediaGalleryImages($product, $additionalImages = array()){
		$gallery = array();
		$images = $this->getReadConnection()->fetchAll($this->_getQuery($product));
		foreach ($images as $key => $image) {
			if(count($gallery) == self::MAX_IMAGES){
				break;
			}
			$url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'catalog'.DS.'product'.$image['value'];
			if($this->_isValid($url)){
				$gallery[] = $url;
			}
		}
		foreach ($additionalImages as $image) {
			if(count($gallery) == self::MAX_IMAGES){
				break;
			}
			if($this->_isValid($url)){
				$gallery[] = $image;
			}
		}
		return $gallery;
	}

}