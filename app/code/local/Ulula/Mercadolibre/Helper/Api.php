<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author    	Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Helper_Api extends Varien_Http_Client{

	const BASE_API_URL = 'https://all2meli.ulula.net/meli/v1/';
	const BASE_API_URL_STAGE = 'https://all2meli.st.ulula.net/meli/v1/';
	const URL_CATEGORY = 'category';
	const URL_CHECK_KEY = 'user/checkkey';
	const URL_ITEMS = 'items';
	const URL_SITEID = 'user/siteid';
	const URL_SYNCHRO_STOCK = 'stock/create';
	const URL_SYNCHRO_PRICE = 'price/create';
	const URL_SYNCHRO = 'synchro/create';
	const URL_STORE = 'category/stores';
	const URL_STORE_ITEM = 'store/create';
	const URL_ORDER_RETRIEVE = 'order/retrieve';

	const LOGFILE = 'ulula_mercadolibre_api.log';

	protected $_params = [];

	protected function _getHeaders(){
		return array(
			'Accept' => 'application/json',
			'Accept-Encoding' => 'gzip,deflate',
			'Content-Type'=>'application/json',
		);
	}

	protected function _getUserAgent() {
		return 'all2meli - '.(string) Mage::helper('ulula_mercadolibre')->getExtensionVersion().
		' - '.Mage::getBaseUrl (Mage_Core_Model_Store::URL_TYPE_WEB). ' - Magento '
		.Mage::getEdition().' '.Mage::getVersion();
	}

	protected function getBaseUrl(){
		$mode = Mage::helper('ulula_mercadolibre')->getMode();
		if($mode == 1){
			return self::BASE_API_URL_STAGE;
		}
		else{
			return self::BASE_API_URL;
		}
	}

	public function checkKey($key){
		$this->_params['key'] = $key;
		return $this->callApi(self::URL_CHECK_KEY, false, false);
	}

	public function getStores($key){
		$this->_params['key'] = $key;
		return $this->callApi(self::URL_STORE, false, false);
	}

	public function getCategory($category_id=''){
		$this->_params = ['category_id' => $category_id, 'attributes' => false];
		$cacheKey = ($category_id == '') ? self::URL_CATEGORY : self::URL_CATEGORY.'_'.$category_id.'_cache';
		return $this->callApi(self::URL_CATEGORY, $cacheKey);
	}

	public function getCategoryAttributes($category_id=''){
		$this->_params = ['category_id' => $category_id, 'attributes' => true];
		$cacheKey = ($category_id == '') ? self::URL_CATEGORY : self::URL_CATEGORY.'_'.$category_id.'_ATTR_cache';
		return $this->callApi(self::URL_CATEGORY, $cacheKey);
	}

	public function getItems(){
		$this->_params = ['auth_key' => Mage::helper('ulula_mercadolibre')->getAll2MeliKey()];
		return $this->callApi(self::URL_ITEMS, false, 120);
	}

	public function getMeliSiteId(){
		return $this->callApi(self::URL_SITEID);
	}

	public function synchroStock($request){
		$this->_params = $request;
		return $this->callApi(self::URL_SYNCHRO_STOCK, false, 0, 'POST');
	}

	public function synchroPrice($request){
		$this->_params = $request;
		return $this->callApi(self::URL_SYNCHRO_PRICE, false, 0, 'POST');
	}

	public function synchroItems($request){
		$this->_params = $request;
		return $this->callApi(self::URL_SYNCHRO, false, 0, 'POST');
	}

	public function synchroStore($request){
		$this->_params = $request;
		return $this->callApi(self::URL_STORE_ITEM, false, 0, 'POST');
	}

	public function orderRetrieve($request){
		$this->_params = $request;
		return $this->callApi(self::URL_ORDER_RETRIEVE, false, 0, 'POST');
	}

	public function callApi($service, $cacheKey = false, $cttl = 3600, $type='GET'){
		$cacheKey = ($cacheKey)? $cacheKey : str_replace('/', '_', $service).'_cache';
		$cache = Mage::app()->getCache();
		if( ($data = $cache->load($cacheKey)) && $cttl){
			return json_decode($data, true);
		}
		else{
			$this->setUri($this->getBaseUrl().$service);
			$headers = $this->_getHeaders();
			$this->config['useragent'] = $this->_getUserAgent();

			$this->setHeaders($headers);
			if(!isset($this->_params['key'])){
				$this->_params['key'] = Mage::helper('ulula_mercadolibre')->getAll2MeliKey();
			}
			if($type == 'GET' ){
				$this->setParameterGet($this->_params);
			}
			elseif( $type == 'POST' ){
				$this->setParameterPost($this->_params);
			}
			$response = $this->request($type);
			// var_dump($response->getBody());die;
			$this->logRequest($type, $service, $headers, $response);
			$this->_params = [];
			$this->resetParameters(true);
			$body = $this->checkResponse($response, $service, $headers);
			if($body && $cttl){
				$cache->save($body, $cacheKey, [], $cttl);
			}
		}
		return json_decode($body, true);
	}

	public function checkResponse($response, $service, $headers){
		if ( $response->isSuccessful() ){
			return $response->getBody();
		}
		else{
			return $this->getResponseMessage($response);
		}
	}

	public function getResponseMessage($response){
		$body = json_decode($response->getBody(),true);
		if(isset($body['message']))
			return Mage::helper('ulula_mercadolibre')->__($body['message']);
		return 'Error: '. $response->getBody();
	}

	public function logRequest($type, $service, $headers, $response){
		if(Mage::helper('ulula_mercadolibre')->isDebug()){
			Mage::log($type.' Service /'.$service, Zend_Log::DEBUG, self::LOGFILE, true);
			Mage::log('Request - headers:'.json_encode($headers), Zend_Log::DEBUG, self::LOGFILE, true);
			Mage::log('Request - parameters:'.json_encode($this->_params), Zend_Log::DEBUG, self::LOGFILE, true);
			Mage::log('Response - headers:'.$response->getHeadersAsString(), Zend_Log::DEBUG, self::LOGFILE, true);
			Mage::log('Response - body:'.$response->getBody(), Zend_Log::DEBUG, self::LOGFILE, true);
		}
	}
}