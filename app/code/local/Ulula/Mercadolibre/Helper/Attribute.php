<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Helper_Attribute extends Mage_Core_Helper_Abstract
{
    const ATTR_PREFIX = 'meli_';
    const ATTR_GROUP = 'Mercadolibre';

    public function setAttributes($meliCategoryId)
    {
        $attributes = $this->getCategoriesAttrFromMeli($meliCategoryId);
        foreach ($attributes as $attribute) {
            if ($this->mustCreateAttribute($attribute) 
                && !$this->attributeExists($this->getAttributeCode($attribute))) {
                $this->createMageAttribute($attribute);
            }
        }

        return $attributes;
    }

    public function getAttributeLabelSuffix()
    {
        return $this->__(' (Mercadolibre)');
    }

    public function mustCreateAttribute($attribute)
    {
        return ($this->isRequired($attribute) || $this->allowVariations($attribute) 
        || $this->hasNoTags($attribute) || $this->isVariationAttribute($attribute));
    }

    public function isRequired($attribute)
    {
        return (
            isset($attribute['tags']) && 
            ( 
                isset($attribute['tags']['required']) || 
                isset($attribute['tags']['catalog_required']) 
            )
        );
    }

    public function isFixed($attribute)
    {
        return (isset($attribute['tags']) && isset($attribute['tags']['fixed']));
    }

    public function attributeExists($code)
    {
        return (bool)Mage::getResourceModel('catalog/eav_attribute')
            ->loadByCode('catalog_product', $code)->getId();
    }

    public function getAttributeCode($attribute, $isVariation = false)
    {
        $attrCode = '';
        if ($this->allowVariations($attribute) || $isVariation) {
            $attrCode = self::ATTR_PREFIX.'variant_'.$attribute['id'];
        } else {
            $attrCode = self::ATTR_PREFIX.'attribute_'.$attribute['id'];
        }

        if (strlen($attrCode) > 30) {
            $attrCode = substr($attrCode, 0, 30);
        }

        return $attrCode;
    }

    public function isProductPk($attribute)
    {
        if (isset($attribute['tags']) && isset($attribute['tags']['product_pk']) 
            && $attribute['tags']['product_pk']) {
                return true;
        }

        return false;
    }

    public function allowVariations($attribute)
    {
        if (isset($attribute['tags']) && isset($attribute['tags']['allow_variations']) 
            && $attribute['tags']['allow_variations']) {
                return true;
        }

        return false;
    }

    public function isVariationAttribute($attribute)
    {
        if (isset($attribute['tags']) && isset($attribute['tags']['variation_attribute']) 
            && $attribute['tags']['variation_attribute'] && 
            !$this->isHidden($attribute) && 
            !$this->isFixed($attribute) ){
                return true;
        }

        return false;
    }

    public function isVariation($attribute){
        return ($this->allowVariations($attribute) && 
            !$this->isHidden($attribute) && 
            !$this->isFixed($attribute)
        );
    }

    public function hasNoTags($attribute)
    {
        return (isset($attribute['tags']) && empty($attribute['tags']));
    }

    public function isHidden($attribute){
        return (
            isset($attribute['tags']) 
            && isset($attribute['tags']['hidden']) 
            && $attribute['tags']['hidden']
        );
    }

    public function getAttributeInput($valueType)
    {
        switch ($valueType) {
            case 'boolean':
                return  'boolean';
            case 'list':
                return  'select';
            default:
                return 'text';
        }
    }

    public function getAttributeType($type)
    {
        switch ($type) {
            case 'boolean':
                return  'int';
            case 'list':
                return  'int';
            default:
                return 'text';
        }
    }

    public function createMageAttribute($attribute)
    {
        $installer = new Mage_Eav_Model_Entity_Setup('core_setup');
        $installer->startSetup();
        $input = $this->getAttributeInput($attribute['value_type']);
        $type = $this->getAttributeType($attribute['value_type']);
        $attributeData = array(
                    'group'           => self::ATTR_GROUP,
                    'label'           => $attribute['name'].$this->getAttributeLabelSuffix(),
                    'input'           => $input,
                    'type'            => $type,
                    'required'        => 0,
                    'visible_on_front'=> 0,
                    'filterable'      => 0,
                    'searchable'      => 0,
                    'comparable'      => 0,
                    'user_defined'    => 1,
                    'is_configurable' => 0,
                    'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                    'note'            => '',        
        );
        if ($attribute['value_type'] == 'list') {
            $attributeData['option'] = array('values' =>  $this->getOptionValues($attribute));
        }    

        if ($attribute['value_type'] == 'boolean') {
            $attributeData['source'] = 'eav/entity_attribute_source_boolean';
        }  

        $installer->addAttribute('catalog_product', $this->getAttributeCode($attribute), $attributeData);
        $installer->endSetup();
    }

    protected function getOptionValues($attribute)
    {
        $values = array();
        foreach ($attribute['values'] as $value) {
            $values[] = $value['name'];
        }

        return $values;
    }

    public function getCategoriesAttrFromMeli($categoryId)
    { 
        $attributes = array();
        $result =  Mage::helper('ulula_mercadolibre/api')->getCategoryAttributes($categoryId);
        if($result)    $attributes = $result['body'];
        return $attributes;
    }

    public function getAttributeValueLabels($attrCode)
    {
        $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $attrCode);
        $labels = array();
          foreach ($attribute->getSource()->getAllOptions(true, true) as $instance) {
            $labels[] = $instance['label'];
          }

        return $labels;
    }

    public function getAttrOptions($attrCode)
    {
        $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $attrCode);
        return $attribute->getSource()->getAllOptions();
    }

    public function getAttributeIdByCode($attrCode)
    {
        $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $attrCode);
        return $attribute->getId();
    }

    public function updateOption($attrCode, $label)
    {
        $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $attrCode);
        $options = $attribute->getSource()->getAllOptions();
        foreach ($options as $option) {
            $storeId = $this->getStoreIdFromLabel($label);
            $optionStoreId = $this->getStoreIdFromLabel($option['label']);
            if ($option['value'] != "" && ($optionStoreId == $storeId) && ($label != $option['value'])) {
                $sql = "UPDATE eav_attribute_option_value SET value =  '".$label."' WHERE option_id =".$option['value'];
                $resource = Mage::getSingleton('core/resource');
                $writeConnection = $resource->getConnection('core_write');
                $writeConnection->raw_query($sql);
            }
        }
    }

    public function getStoreIdFromLabel($label)
    {
        $labelArray = explode('-', $label);
        return trim(str_replace(array('[',']'), '', $labelArray[0]));
    }

    public function deleteOption($attrCode, $label)
    {
        $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $attrCode);
        $options = $attribute->getSource()->getAllOptions();
        $optionsDelete = array();
        foreach ($options as $option) {
            if ($option['value'] != "" && $option['label'] == $label) {
                $optionsDelete['delete'][$option['value']] = true;
                $optionsDelete['value'][$option['value']] = true;
            }
        }

        $installer = new Mage_Eav_Model_Entity_Setup('core_setup');
        $installer->addAttributeOption($optionsDelete);
    }
}