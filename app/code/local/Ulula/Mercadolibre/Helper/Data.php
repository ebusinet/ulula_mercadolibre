<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author    	Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Helper_Data extends Mage_Core_Helper_Abstract
{
	const API_URL = 'https://api.mercadolibre.com/'; 
	const LOG_FILE_NAME = 'ulula_mercadolibre.log';

	public function getExtensionVersion()
	{
	    return (string) Mage::getConfig()->getNode()->modules->Ulula_Mercadolibre->version;
	}

	public function getAll2MeliKey($storeId = null){
		return Mage::getStoreConfig('mercadolibre/configuration/all2meli_key', $storeId);
	}

	public function isValidAuthKey($authkey){
   		return ($authkey == $this->getAll2MeliKey());
   	}

   	public function isSynchroEnabled($syncType){
   		return (bool)Mage::getStoreConfig('mercadolibre/synchronization/'.$syncType.'_enabled');
   	}

   	public function getStockOffset(){
   		return (int)Mage::getStoreConfig('mercadolibre/synchronization/stock_offset');
   	}

   	public function getMeliStoreId($storeId = null){
		return Mage::getStoreConfig('mercadolibre/configuration/meli_store_id', $storeId);
	}

	public function isDebug(){
		return Mage::getStoreConfig('mercadolibre/configuration/debug');
	}

	public function getMode(){
		return (int)Mage::getStoreConfig('mercadolibre/configuration/mode');
	}

   	/**
   	 * Returns Mercadolibe Group Price Name
   	 * @return string 
   	 */
   	public function getGroupPriceName(){
		return Mage::getStoreConfig('mercadolibre/synchronization/group_price');
	}

	public function log($action, $data){
		Mage::log($action, null, self::LOG_FILE_NAME, true);
		Mage::log($data, null, self::LOG_FILE_NAME, true);
	}

	public function getMeliCategory($product){
		if($product->getMeliCategoryId()) {
			return $this->getMeliCategoryById($product->getMeliCategoryId());
		}
		$meliCategory = $this->getMeliCategoryFromCategories($product);
		if($meliCategory) return $meliCategory;
		return $this->getMeliCategoryFromParent($product);
		
	}

	protected function getMeliCategoryFromCategories($product){
		$categoryIds = $this->getProductCategoriesIds($product);
		foreach ($categoryIds as $categoryId) {
			$meliCategory = $this->getMeliCategoryByCategoryId($categoryId);
			if($meliCategory->getMeliCategoryId()) return $meliCategory;
		}
		return false;
		
	}

	protected function getMeliCategoryFromParent($product){
		if($product->getTypeId() == "simple"){
		    $parentIds = Mage::getModel('catalog/product_type_grouped')->getParentIdsByChild($product->getId());
		    if(!$parentIds)
		        $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($product->getId());
		
			foreach ($parentIds as $parentId) {
				$parentProduct = Mage::getModel('catalog/product')->load($parentId);
				return $this->getMeliCategory($parentProduct);
			}
		}
		return Mage::getModel('ulula_mercadolibre/category');
	}

	protected function getMeliCategoryById($categoryId){
		return  Mage::getModel('ulula_mercadolibre/category')->load($categoryId,'meli_category_id');
	}

	protected function getMeliCategoryByCategoryId($categoryId){
		return  Mage::getModel('ulula_mercadolibre/category')->load($categoryId,'mage_category_id');
	}

	protected function getProductCategoriesIds($product){
		return $product->getResource()->getCategoryIds($product);
	}

	protected function _clearSyncJob(){
		$collection = Mage::getModel('cron/schedule')->getCollection()
			->addFieldToFilter('job_code', 'ulula_mercadolibre_sync')
			->addFieldToFilter('status', Mage_Cron_Model_Schedule::STATUS_PENDING);
		if($collection)
			$collection->walk('delete');
	}

	public function createSyncJob(){
		$this->_clearSyncJob();
        $ts = strftime('%Y-%m-%d %H:%M:00', time());
        try {
            $schedule = Mage::getModel('cron/schedule');
            $schedule->setJobCode('ulula_mercadolibre_sync')
                    ->setCreatedAt($ts)
                    ->setScheduledAt($ts)
                    ->setStatus(Mage_Cron_Model_Schedule::STATUS_PENDING)
                    ->save();
        }
        catch (Exception $e) {
            throw new Exception(Mage::helper('cron')->__('Unable to sync your products.'));
        }
    }

    protected function getReadConnection(){
    	$resource = Mage::getSingleton('core/resource');
		return $resource->getConnection('core_read');
    }
}