<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Adminhtml_MercadolibreController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')
            ->isAllowed('mercadolibre/item');
    }
    
    public function getItemsAction()
    {
        $_helper = Mage::helper('ulula_mercadolibre');;
        $result = Mage::helper('ulula_mercadolibre/api')->getItems();
        $this->truncateItemsTable();
        if(isset($result['code'])){
            $message = $_helper->__('Error geting your publications, check your key');
        }
        elseif($result['body']['total']){
            $message = $_helper->__('%s publications were found. Please, wait 2 minutes until synchronization is completed.', $result['body']['total']);
        }
        else{
             $message = $_helper->__('No publications found.');
        }
        Mage::app()->getResponse()->setBody($message);
    }

    protected function truncateItemsTable(){
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $query = 'TRUNCATE TABLE ulula_mercadolibre_item;';
        $writeConnection->query($query);
    }

    protected function setMeliData($product, $meliData){
        $changed = false;
        foreach ($meliData['product'] as $key => $value) {
            if($value != null){
                if($product->getData($key) != $value){
                    $product->setData($key, $value);
                    $changed = true;
                }
            }
        }
        if($changed) $product->save();
    }

    /**
     * Publish product in meli
     * 
     * @return string result of the publish
     */
    public function publishAction()
    {
        $_helperProduct = Mage::helper('ulula_mercadolibre/product');
        $pid = $_POST['pid'];
        $product = Mage::getModel('catalog/product')->load($pid);
        parse_str($_POST['meli_data'],$meliData);
        $this->setMeliData($product, $meliData);
        if($_helperProduct->canPublish($product)){
            $request = $_helperProduct->getRequest($product);
            $hasStock = $this->checkStockBeforePublish($request['in_data']);
            if($hasStock){
                $item = Mage::getModel('ulula_mercadolibre/item')->load($product->getSku(),'sku');
                if ($item->getId()) {
                     echo Mage::helper('ulula_mercadolibre')->__('Product has been already publish, please wait a few minutes for the sync to complete.');
                } else {
                    $item->setTitle($_helperProduct->getTitle($product));
                    $item->setProductId($product->getId());
                    $item->setSku($product->getSku());
                    $item->setStatus('pendiente');
                    $item->save();
                    Mage::helper('ulula_mercadolibre/api')->synchroItems($request);
                    echo 'true';
                }
            }
            else{
                echo Mage::helper('ulula_mercadolibre')->__('Cannot publish, product has no stock');
            }
            
        }
        else{
            echo Mage::helper('ulula_mercadolibre')->__('Cannot publish, check the category and required fields');
        }
    }

    public function syncAction()
    {
        $_helperProduct = Mage::helper('ulula_mercadolibre/product');
        $pid = $_POST['pid'];
        $product = Mage::getModel('catalog/product')->load($pid);
        parse_str($_POST['meli_data'],$meliData);
        $this->setMeliData($product, $meliData);
        if($_helperProduct->canSync($product)){
            $item = Mage::getModel('ulula_mercadolibre/item')->load($product->getSku(),'sku');
            $request = $_helperProduct->getRequest($product, $item->getMeliId());
            $item->setTitle($product->getMeliTitle());
            $item->setProductId($product->getId());
            $item->setSku($product->getSku());
            $item->setStatus('synchronizing');
            $item->save();
            $request['meli_pub_id'] = $item->getMeliId();
            Mage::helper('ulula_mercadolibre/api')->synchroItems($request);
            if($product->getMeliStores()){
                $storeOptionsArray = explode(',', $product->getMeliStores());
                $storeIds = Mage::helper('ulula_mercadolibre/store')->getMeliStoreIds($storeOptionsArray);
                $storeRequest['store_data'] = json_encode(array(
                    'item' => $item->getMeliId(),
                    'stores' => $storeIds
                ));
                $storeRequest['availability_time_in_hours'] = 48;
                $response = Mage::helper('ulula_mercadolibre/api')->synchroStore($storeRequest);
            }
            echo 'true';
        }
        else{
            echo Mage::helper('ulula_mercadolibre')->__('Cannot sync, check the category and required fields');
        }
    }

    protected function checkStockBeforePublish($data){
        $dataArray = json_decode($data, true);
        $qty = 0;
        if(isset($dataArray['variants'])){
            foreach ($dataArray['variants'] as $variant) {
                $qty += $variant['qty'];
            }
        }
        $qty += $dataArray['qty'];
        return (bool)$qty;
    }
}