<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Adminhtml_ItemController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')
            ->isAllowed('mercadolibre/item');
    }

    public function indexAction()
    {
        $this->_title($this->__('Mercadolibre'))->_title($this->__('Publications'));
        $this->loadLayout();
        $this->_setActiveMenu('mercadolibre/item');
        $this->_addContent($this->getLayout()->createBlock('ulula_mercadolibre/adminhtml_mercadolibre_item'));
        $this->renderLayout();
    }

    public function editAction()
    {
        $itemId = $this->getRequest()->getParam('id');
        $itemModel = Mage::getModel('ulula_mercadolibre/item')->load($itemId);
          
           if ($itemModel->getId() )
           {
             Mage::register('item_data', $itemModel);
             $this->loadLayout();
             // $this->_setActiveMenu('ulula_mercadolibre/set_time');
             $this->_addBreadcrumb('Mercadolibre - Publicaciones', 'Manager');
             $this->_addBreadcrumb('Publicaciones Description', 'Publicaciones Description');
             $this->getLayout()->getBlock('head')
                  ->setCanLoadExtJs(true);
             $this->_addContent($this->getLayout()
                  ->createBlock('ulula_mercadolibre/adminhtml_mercadolibre_item_edit'))
                  ->_addLeft($this->getLayout()
                  ->createBlock('ulula_mercadolibre/adminhtml_mercadolibre_item_edit_tabs')
              );
             $this->renderLayout();
           }
           else
           {
                 Mage::getSingleton('adminhtml/session')->addError('Publication does not exist');
                 $this->_redirect('*/*/');
            }
       }
 
    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('ulula_mercadolibre/adminhtml_mercadolibre_item_grid')->toHtml()
        );
    }
 
    public function exportItemCsvAction()
    {
        $fileName = 'meli_publications.csv';
        $grid = $this->getLayout()->createBlock('ulula_mercadolibre/adminhtml_mercadolibre_item_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }
 
    public function exportItemExcelAction()
    {
        $fileName = 'meli_publications.xml';
        $grid = $this->getLayout()->createBlock('ulula_mercadolibre/adminhtml_mercadolibre_item_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    public function massSyncAction()
    {
        $itemIds = (array)$this->getRequest()->getParam('item');
        $sync = (int)$this->getRequest()->getParam('sync');   

        if(!is_array($itemIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select publication(s).'));
        } 
        else {
            try {
                $model = Mage::getModel('ulula_mercadolibre/item');
                foreach ($itemIds as $itemId) {
                    $model->load($itemId)->setSync($sync)->save();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__('Total of %d record(s) were updated.', count($itemIds))
                );
            } 
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
         
        $this->_redirect('*/*/index');
    }

    public function massStatusAction()
    {
        $itemIds = (array)$this->getRequest()->getParam('item');
        $status = $this->getRequest()->getParam('status');   

        if(!is_array($itemIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select publication(s).'));
        } 
        else {
            try {
                $model = Mage::getModel('ulula_mercadolibre/item');
                foreach ($itemIds as $itemId) {
                    $model = $model->load($itemId);
                    if(!$model->getMeliId())
                        continue;
                    if($status != $model->getStatus()){
                        $inData = array();
                        if($status == 'delete'){
                            $model->delete();
                            $inData = array('status'=>'closed','deleted'=>'true');
                        }
                        elseif($status == 'paused'){
                            if($model->getStatus() == 'active'){
                                $inData = array('status'=>'paused');
                            }
                            else{
                                $error = Mage::helper('ulula_mercadolibre')->__('Cannot pause a closed item.');
                                Mage::throwException($error);
                            }
                        }
                        else{
                            $inData = array('status'=>$status);
                        }
                        $model->setStatus($status)->save();
                        $request['key'] = Mage::helper('ulula_mercadolibre')->getAll2MeliKey();
                        $request['meli_pub_id'] = $model->getMeliId();
                        $request['sku'] = $model->getSku();
                        $request['in_data'] = json_encode($inData);
                        Mage::helper('ulula_mercadolibre/api')->synchroItems($request);
                    }
                    
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__('Total of %d record(s) were updated.', count($itemIds))
                );
            } 
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
         
        $this->_redirect('*/*/index');
    }

    public function synchronizeAction(){
       Mage::helper('ulula_mercadolibre')->createSyncJob();
       $message = $this->__('Synchronization successfully started, please wait about 2 minutes to see the changes in Mercadolibre.');
       Mage::getSingleton('core/session')->addSuccess($message);
       $this->_redirectReferer();
    }

    public function saveAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            
            $model = Mage::getSingleton('ulula_mercadolibre/item')->load($postData['id']);

            $model->setSku($postData['sku']);
            if (isset($postData['variation']))
                $model->updateSkuVariants($postData['variation']);
            try {
                $model->save();
 
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The Item has been saved.'));
                $this->_redirect('*/*/');
                $this->syncSkus($model);
                return;
            }  
            catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while saving this item.'));
            }
            $this->_redirectReferer();
        }
    }

    protected function syncSkus($item){
        $storeId = ($product = $item->getProduct()) ? 
            Mage::helper('ulula_mercadolibre/product')->getProductStoreId($product): 
            null;
        $request['key'] = Mage::helper('ulula_mercadolibre')->getAll2MeliKey($storeId);
        $request['meli_pub_id'] = $item->getMeliId();
        $request['sku'] = $item->getSku();
        $info = json_decode($item->getInfo(), true);
        $request['in_data']['seller_custom_field'] = $item->getSku();
        foreach ($info['variations'] as $variation) {
            $request['in_data']['variations'][] =
                 array('id'=> $variation['id'], 'seller_custom_field' => $variation['seller_custom_field']);
        }
        $request['in_data'] = json_encode($request['in_data']);
        Mage::helper('ulula_mercadolibre/api')->synchroItems($request);
    }

}