<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */


 class Ulula_Mercadolibre_Adminhtml_Meli_Catalog_ProductController  extends Mage_Adminhtml_Controller_Action{

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')
            ->isAllowed('mercadolibre/meli_product');
    }
    
 	public function indexAction()
    {
        $this->_title($this->__('ulula_mercadolibre'))->_title($this->__('Mercadolibre - Manage Products'));
        $this->loadLayout();
        $this->_setActiveMenu('catalog/meli_product');
        $this->_addContent($this->getLayout()->createBlock('ulula_mercadolibre/adminhtml_Meli_catalog_product'));
        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('ulula_mercadolibre/adminhtml_Meli_catalog_product_grid')->toHtml()
        );
    }
 
    public function exportInchooCsvAction()
    {
        $fileName = 'meli_products.csv';
        $grid = $this->getLayout()->createBlock('ulula_mercadolibre/adminhtml_Meli_catalog_product_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }
 
    public function exportInchooExcelAction()
    {
        $fileName = 'meli_products.xml';
        $grid = $this->getLayout()->createBlock('ulula_mercadolibre/adminhtml_Meli_catalog_product_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    public function massPublishAction(){
        $pids = $this->getRequest()->getParam('product');
        if(!is_array($pids)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('ulula_mercadolibre')->__('Please select product(s).'));
        } 
        else {
            try {
                $productCollection = Mage::getModel('catalog/product')->getCollection()
                    ->addFieldToFilter('entity_id', array('in'=> $pids));
                foreach ($productCollection as $product) {
                    $product->setMeliPublish(1);
                    $product->save();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ulula_mercadolibre')->__(
                    'Total of %d record(s) were enabled to be published.', count($pids)
                    )
                );
            } 
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
 
        $this->_redirect('*/*/index');
    }

    public function massUnpublishAction(){
        $pids = $this->getRequest()->getParam('product');
        if(!is_array($pids)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('ulula_mercadolibre')->__('Please select product(s).'));
        } 
        else {
            try {
                $productCollection = Mage::getModel('catalog/product')->getCollection()
                    ->addFieldToFilter('entity_id', array('in'=> $pids));
                foreach ($productCollection as $product) {
                    $product->setMeliPublish(0);
                    $product->save();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ulula_mercadolibre')->__(
                    'Total of %d record(s) were disabled to be published.', count($pids)
                    )
                );
            } 
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
 
        $this->_redirect('*/*/index');
        
    }

    public function massListAction(){
        $pids = $this->getRequest()->getParam('product');
        if(!is_array($pids)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('ulula_mercadolibre')->__('Please select product(s).'));
        } 
        else {
            try {
                    $productCollection = Mage::getModel('catalog/product')->getCollection()
                        ->addFieldToFilter('entity_id', array('in'=> $pids));

                    $_helperProduct = Mage::helper('ulula_mercadolibre/product');
                    $published = 0;
                    $key = Mage::helper('ulula_mercadolibre')->getAll2MeliKey();
                    foreach ($productCollection as $product) {
                        $product = $product->load($product->getId());
                        if($_helperProduct->canPublish($product)){
                            $poductData = $_helperProduct->getProductJson($product);
                            $request['key'] = $key;
                            $request['meli_pub_id'] = null;
                            $request['sku'] = $product->getSku();
                            $request['in_data'] = $poductData;
                            $item = Mage::getModel('ulula_mercadolibre/item')->load($product->getSku(),'sku');
                            $item->setTitle($product->getMeliTitle());
                            $item->setProductId($product->getId());
                            $item->setSku($product->getSku());
                            $item->setStatus('pendiente');
                            $item->save();
                            Mage::helper('ulula_mercadolibre/api')->synchroItems($request);
                            $published++;
                        }
                        
                    }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ulula_mercadolibre')->__(
                    'Total of %d product(s) are going to be published in mercadolibre.', $published
                    )
                );
            } 
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
 
        $this->_redirect('*/*/index');
        
    }

    public function massSyncAction(){
        $pids = $this->getRequest()->getParam('product');
        if(!is_array($pids)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('ulula_mercadolibre')->__('Please select product(s).'));
        } 
        else {
            try {
                    $productCollection = Mage::getModel('catalog/product')->getCollection()
                        ->addFieldToFilter('entity_id', array('in'=> $pids));

                    $_helperProduct = Mage::helper('ulula_mercadolibre/product');
                    $sync = 0;
                    $key = Mage::helper('ulula_mercadolibre')->getAll2MeliKey();
                    foreach ($productCollection as $product) {
                        $product = $product->load($product->getId());
                        if($_helperProduct->canSync($product)){
                            $item = Mage::getModel('ulula_mercadolibre/item')->load($product->getSku(),'sku');
                            if(!$item->getMeliId()) continue;
                            $request = $_helperProduct->getRequest($product, $item->getMeliId());
                            $item->setTitle($product->getMeliTitle());
                            $item->setProductId($product->getId());
                            $item->setSku($product->getSku());
                            $item->setStatus('synchronizing');
                            $item->save();
                            Mage::helper('ulula_mercadolibre/api')->synchroItems($request);
                            if($product->getMeliStores()){
                                $storeOptionsArray = explode(',', $product->getMeliStores());
                                $storeIds = Mage::helper('ulula_mercadolibre/store')->getMeliStoreIds($storeOptionsArray);
                                $storeRequest['store_data'] = json_encode(array(
                                    'item' => $item->getMeliId(),
                                    'stores' => $storeIds
                                ));
                                $storeRequest['availability_time_in_hours'] = 48;
                                $response = Mage::helper('ulula_mercadolibre/api')->synchroStore($storeRequest);
                            }
                            $sync++;
                        }
                        
                    }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ulula_mercadolibre')->__(
                    'Total of %d product(s) are going to be synchronized in mercadolibre.', $sync
                    )
                );
            } 
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
 
        $this->_redirect('*/*/index');
        
    }
}