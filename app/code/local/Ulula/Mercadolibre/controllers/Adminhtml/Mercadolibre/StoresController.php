<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Adminhtml_Mercadolibre_StoresController extends Mage_Adminhtml_Controller_Action
{

	protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')
            ->isAllowed('mercadolibre/stores');
    }

    public function indexAction()
    {
    	$_helper = Mage::helper('ulula_mercadolibre/attribute');
    	if(!$_helper->attributeExists('meli_stores')){
    		$this->createStoreAttribute();
    	}
    	else{
    		//$this->checkOptions();
    	}
        $this->_title($this->__('Mercadolibre'))->_title($this->__('Stores'));
        $this->loadLayout();
        $this->_setActiveMenu('mercadolibre/stores');
        $this->_addContent($this->getLayout()->createBlock('ulula_mercadolibre/adminhtml_mercadolibre_stores'));
        $this->renderLayout();
    }

    protected function checkOptions(){
    	$labels = Mage::helper('ulula_mercadolibre/attribute')->getAttributeValueLabels($attrCode);
    	$storeIds = $this->getStoresId($labels);
    	$stores = Mage::helper('ulula_mercadolibre/api')->getStores(Mage::helper('ulula_mercadolibre')->getAll2MeliKey());
    }

    

    protected function createStoreAttribute(){
		$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
		$installer->startSetup();
		$attributeData = array(
		            'group'           => 'Mercadolibre',
		            'label'           => 'Sucursales '.Mage::helper('ulula_mercadolibre/attribute')->getAttributeLabelSuffix(),
		            'input'           => 'multiselect',
		            'type'            => 'text',
		            'backend'         => 'eav/entity_attribute_backend_array',
		            'required'        => 0,
		            'visible_on_front'=> 0,
		            'filterable'      => 0,
		            'searchable'      => 0,
		            'comparable'      => 0,
		            'user_defined'    => 1,
		            'is_configurable' => 0,
		            'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		            'note'            => '',        
		);
		$stores = Mage::helper('ulula_mercadolibre/api')->getStores(Mage::helper('ulula_mercadolibre')->getAll2MeliKey());
	    $attributeData['option'] = array('values' =>  $stores);	
		$installer->addAttribute('catalog_product', 'meli_stores', $attributeData);
		$installer->endSetup();
	}

}