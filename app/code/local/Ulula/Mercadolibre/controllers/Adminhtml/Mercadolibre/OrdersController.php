<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Adminhtml_Mercadolibre_OrdersController extends Mage_Adminhtml_Controller_Action
{

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')
            ->isAllowed('mercadolibre/orders');
    }

    public function indexAction()
    {
        $this->_title($this->__('Mercadolibre'))->_title($this->__('Orders'));
        $this->loadLayout();
        $this->_setActiveMenu('mercadolibre/order');
        $this->_addContent($this->getLayout()->createBlock('ulula_mercadolibre/adminhtml_mercadolibre_order'));
        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('ulula_mercadolibre/adminhtml_mercadolibre_order_grid')->toHtml()
        );
    }
 
    public function exportIemCsvAction()
    {
        $fileName = 'meli_orders.csv';
        $grid = $this->getLayout()->createBlock('ulula_mercadolibre/adminhtml_mercadolibre_order_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }
 
    public function exportorderExcelAction()
    {
        $fileName = 'meli_orders.xml';
        $grid = $this->getLayout()->createBlock('ulula_mercadolibre/adminhtml_mercadolibre_order_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    public function retriveAction()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        if ($orderId) {
            $meliOrder = Mage::getModel('ulula_mercadolibre/order')->load($orderId, 'meli_order_id');
            if ($meliOrder->getId() && !$meliOrder->getOrderId() && $meliOrder->getMeliOrderData()) {
                $response = Mage::helper('ulula_mercadolibre/api')->orderRetrieve(array('order_id'=>$orderId));
                if($response){
                    $meliOrder->setMeliOrderData('');
                    $meliOrder->save();
                } else {
                    Mage::getSingleton('adminhtml/session')
                        ->addError($this->__('Cannot retrieve Order from meli'));
                }
            }
            else {
                Mage::getSingleton('adminhtml/session')
                    ->addError($this->__('Cannot retrieve Order from meli'));
            }
        } else {
            Mage::getSingleton('adminhtml/session')
                ->addError($this->__('Invalid Order Id'));
        }

        $this->_redirect('*/*');
    }
}